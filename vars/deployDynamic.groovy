#!/usr/bin/env groovy

def call(isStart) {
  
  parallel (
     "docker-composer": { 
         println("Start executing stage 1")
         sh "echo p1; sleep 20s; echo phase1" 
         println("Finish executing stage 1")
     },
     "k8s": { 
         if (isStart){
             println("Start executing stage 2")
             sh "echo p2; sleep 10s; echo phase2" 
             println("Finish executing stage 2")
         }
     }
   )
  
  if (isStart) {
    println("Start")
  }
}