package org.jenkins

import groovy.json.JsonSlurper
import groovy.transform.Field


println("Nothing to do")


def run(String appName, String specific_data, String custom_environ) {
    println("Inside")

    final def runStage = { String stageName, Closure callback ->
        stage(stageName) {
          try {
            callback()
          } catch (e) {
            error(e.getMessage())
          }
        }
      }



  node() {

    runStage('Workspace Cleanup') {
      // Wipe the workspace so we are building completely clean
      println("Wipe workspace")
    }
    
    runStage('Deployment') {
      // Wipe the workspace so we are building completely clean
      println("Wipe workspace")
      isStart=false
      deployDynamic(isStart)
    }
    
    runStage('Finalizing') {
      // Wipe the workspace so we are building completely clean
      println("Wipe workspace")
    }
  }
}